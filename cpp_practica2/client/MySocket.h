//#ifndef __MYSOCKET_H__
//#define __MYSOCKET_H___


// MySocket.h esta definida para ser utilizada por client.cpp

class MySocket;

#include <iostream>
#include <cstdio> // c library for stdio.h (printf, scanf)
#include <string.h>

/* connect, create socket and send */
#include <sys/types.h>
#include <sys/socket.h>

/* for errors catching */
#include <errno.h>

/* read from socket */
#include <unistd.h> 

#include <arpa/inet.h>


using namespace std;

class MySocket {
private:
  int    socket_fd;
  struct sockaddr_in server;
  
public:
  MySocket() {
    /* nothing here */
  }

  MySocket(char *ip, int port) throw (int){ /* errno */
    setIp(ip);
    setPort(port);
    setFamily();
    createSocket();
    connectSocket();
  }

  /* server connection configuration methods */

  void setIp(char *ip) {server.sin_addr.s_addr = inet_addr(ip);}
  
  void setPort(int port) {server.sin_port = htons( port );}
  
  void setFamily() {server.sin_family = AF_INET;}

  int getSocketfd() {return socket_fd;}

  //create, connect, send and receive methods

  void createSocket() throw (int) {
    if ((socket_fd = socket(AF_INET , SOCK_STREAM , 0)) < 0)
      throw errno;
  }

  void connectSocket() throw (int) {
    if (connect(socket_fd, (struct sockaddr *)&server, sizeof(server)) < 0)
      throw errno;
  }

  void print(string msg) throw (int) {
    if (send(MySocket::socket_fd, msg.c_str(), strlen(msg.c_str()), 0) < 0)
      throw errno;
  }
  
  void println(string msg) throw (int) {
    print(msg + "\n");
  }

  //returns size of string (linux$man read)
  int readLine(char* recvBuff) throw (int) {
    memset(recvBuff, '0', sizeof(recvBuff)); // initialise buffer to all zeros
    int n = read(this->socket_fd, recvBuff, sizeof(recvBuff)-1);
    if (n < 0)
      throw errno;
    return n;
  }
  
  void close() throw (int){
    if (shutdown(socket_fd,0) < 0)
      throw errno;
  }
};
