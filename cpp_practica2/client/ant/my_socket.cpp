/*
  Filename: my_socket.cpp
  Purpose: 
  Author: Gonzalo De Benito
  Date: 2017 May 1
*/

#include <iostream>
#include <cstdio> // c library for stdio.h (printf, scanf)
#include <string.h>

/* connect, create socket and send */
#include <sys/types.h>
#include <sys/socket.h>

/* for errors catching */
#include <errno.h>

/* read from socket and close */
#include <unistd.h>

#include <arpa/inet.h>


using namespace std;

class my_socket   //MySocket
{
private:
  int    socket_fd;
  struct sockaddr_in server;
  
public:
  my_socket ()
  {
    /* nothing here */
  }

  my_socket (char *ip, int port) throw (int) /* errno */
  {
    set_ip(ip);
    set_port(port);
    set_family();
    create_socket();
    connect_socket();
  }

  /* server connection configuration methods */

  void set_ip (char *ip) //setIp
  {
    server.sin_addr.s_addr = inet_addr(ip);
  }
  
  void set_port (int port)
  {
    server.sin_port = htons( port );
  }
  
  void set_family ()
  {
    //IPv4
    server.sin_family = AF_INET;
  }

  int get_socket_fd (){
    return socket_fd;
  }

  //create, connect, send and receive methods

  void create_socket () throw (int)
  {
    if ((socket_fd = socket(AF_INET , SOCK_STREAM , 0)) < 0)
      throw errno;
  }

  void connect_socket () throw (int)
  {
    if (connect(socket_fd, (struct sockaddr *)&server, sizeof(server)) < 0)
      throw errno;
  }

  void print (string msg) throw (int)
  {
    //send message from string
    if (send(my_socket::socket_fd, msg.c_str(), strlen(msg.c_str()), 0) < 0)
      throw errno;
  }
  
  void println (string msg) throw (int)
  {
    print(msg + "\n");
  }

  //returns size of string (linux$man read)
  int readline (char* recvBuff) throw (int)
  {
    memset(recvBuff, '0', sizeof(recvBuff)); // initialise buffer to all zeros
    int n = read(this->socket_fd, recvBuff, sizeof(recvBuff)-1);
    if (n < 0)
      throw errno;
    return n;
  }

  void close () throw (int)
  {
    if (shutdown(socket_fd,0) < 0)
      {
	throw errno;
      }
  }
  
};

int main ()
{
  try
    {
      my_socket ms ("127.0.0.1",2222);
      char recvBuff[1024];
      int n = 0; //EOF

      /* RECEIVE (Console Thread) */
      /*while ((n = ms.readline(recvBuff)) > 0)
	{
	  recvBuff[n] = 0; //end of file at length n
	  // fputs, fwrite or fprintf write chain into stream
	  if(fputs(recvBuff, stdout) == EOF)
	    {
	      cout << "\n Error : Fputs error" << endl;
	    }
	 }
      */

      /* SEND (Keybord Thread) */
      ///*
      string str_input;
      while (true)
	{
	  //cin >> read we cannot use because gets string split by whitespace
	  getline(cin, str_input);

	  if (cin.eof()) //or str_input==""
	    {
	      ms.close();
	      return 0;
	      //or shutdown(ms.socket_fd,1);
	    }
     
	  else
	    {
	      ms.println(str_input);
	    }
	
        }
      //*/
      return 0;
    }
  
  catch (int e)
    {
      cout << strerror(e) << endl;
      return 1;
    }
}


//useful
/*
  try
  {
  create_socket();
  connect_socket();
  }
  catch (int e)
  {
  cout << strerror(e) << endl;
  }
*/


// cosas a comentar en la presentación
/*
 man sockets 
  read
  send
  connect
  socket()
  socket_file_descriptor
  errno
  std::cout
  this->
  my_socket::
  
*/
