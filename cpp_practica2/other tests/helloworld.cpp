/*
  File-Name:helloworld.cpp
  Purpose: Learning basics from cpp
  Author: Gonzalo De Benito
*/

#include <iostream>

int main() {
  std::cout << "Hello World!" << std::endl;
  //is the same as
  std::cout << "Hello World!\n";
  return 0;
}
