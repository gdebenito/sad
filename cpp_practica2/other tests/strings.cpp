/*
  File-Name: strings.cpp
  Purpose: Learning basics from cpp
  Author: Gonzalo De Benito
*/

//std
#include <iostream>
#include <string>

int main()
{
  //const declaration
  const std::string s = "Hello";
  //  s = "Hello"; INCORRECT s is const
  const std::string t = s;
  char c = 'o';
  const std::string z(5, c);

  // string print
  std::cout << "s is:" <<  s << std::endl;
  std::cout << "t is:" <<  t << std::endl;
  std::cout << "z is:" <<  z << std::endl;
  std::cout << "input:";

  //string read
  std::string read;
  std::cin >> read;

  //read print
  std::cout << "output:" << read << std::endl;
  
  //end
  return 0;
}
