/*
  Filename: sockets_test.c
  Purpose: Test Sockets in C
  Author: Gonzalo De Benito
  Date: 2017 Apr 31
*/

#include<stdio.h>
#include<string.h>    //strlen
#include<sys/socket.h>
#include<arpa/inet.h> //inet_addr

// with netcat: $nc -lp2222
 
int main(int argc , char *argv[])
{
    int socket_desc;
    struct sockaddr_in server;
    char *message;
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }

    char *ip = "127.0.0.1";
    int port = 2222;
    
    /*
    server.sin_addr.s_addr = inet_addr("127.0.0.1"); //set ip add
    server.sin_family = AF_INET;
    server.sin_port = htons( 2222 ); //set port
    */
    
    server.sin_addr.s_addr = inet_addr(ip); //set ip add
    server.sin_family = AF_INET;
    //http://stackoverflow.com/questions/4976897/what-is-address-family
    // AF_INET = IPv4
    server.sin_port = htons( port ); //set port
 
    //Connect to remote server
    if (connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        puts("connect error");
        return 1;
    }
     
    puts("Connected\n");
     
    //Send some data
    message = "Hello world";
    if( send(socket_desc , message , strlen(message) , 0) < 0)
      {
        puts("Send failed");
        return 1;
      }
    
    puts("Data Send\n");
     
    return 0;
}
