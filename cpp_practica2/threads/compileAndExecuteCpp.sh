#!/bin/sh

# File-Name: compileAndExecuteCpp.sh
# Purpose: help and remember how to compile cpp programs
# Author: Gonzalo De Benito
# Data input: program name
# Data output: compiled program

if [ -n $1 ] #non-zero length
then
    g++ $1 -std=c++11 -lpthread #for threads
    ./a.out
else
    echo "Please input valid argument"
fi
