#include <iostream>
#include <cstdlib>
#include <pthread.h>

using namespace std;

#define NUM_THREADS     5

void *PrintHello(void *threadid) {
   long tid;
   tid = (long)threadid;
   cout << "{ID_" << tid << "}";
   for(int i = 0; i < 10000; i++)
     if(i%1000==0)
       cout << tid;
   pthread_exit(NULL);
}

int main () {
   pthread_t threads[NUM_THREADS];
   int rc;
   int i;
	
   for( i=0; i < NUM_THREADS; i++ ){
      cout << "{NEW_" << i << "}";
      rc = pthread_create(&threads[i], NULL, PrintHello, (void *)i);
		
      if (rc){
         cout << "{ERR_" << rc << "}";
         exit(-1);
      }
   }
   pthread_exit(NULL);
}
