/*
  File-Name:threads_text.cpp
  Purpose: Learning basics from threads
  Author: Gonzalo De Benito
*/

//prefix std
#include <iostream>  //Standard Input / Output Streams Library
#include <thread>   //threads lib

//for including threads is needed -lpthread option on compiler

void sum(int x, int y)
{
  std::cout << x+y << std::endl;
}

void multiply(int x, int y)
{
  std::cout << x*y << std::endl;
}

void thread1load()
{
  for(int i = 0; i < 100; i++)
    std::cout << "_";
}

void thread2load()
{
  for(int i = 0; i < 200; i++)
    std::cout << "=";
}

int main()
{
  //main function

  //start running threads
  std::thread first(thread1load);
  std::thread second(thread2load);

  first.join();     /* wait until thread first is finished */

  std::cout << "1 end ";

  second.join();     /* wait until thread first is finished */

  std::cout << "2 end " << std::endl << std::endl;

  
  std::thread sum1(sum, 9, 9);
  std::thread sum2(sum, 6, 8);
  sum1.join();
  sum2.join();
  
  std::thread mult1(multiply, 9, 9);
  mult1.join();
  
  std::cout << "end" << std::endl;
  
  return 0;
}
