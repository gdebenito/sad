package practica2;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import practica2.MyServerSocket;
import practica2.MySocket;



public class Client {

    public static void main(String[] args){
	
	try{
	    MySocket s = new MySocket( args[0], Integer.parseInt(args[1]));
	    
	    //keyboard thread
	    new Thread(){
		public void run(){
		    String line;
		    try{
			BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
			while((line = bf.readLine() ) != null){
			    s.println(line);
			}
			s.close();
		    } catch (IOException ex){
		    }
		}
	    }.start();

	    //console thread
	    new Thread(){
		public void run(){
		    String line;
		    try{
			while((line = s.readLine()) != null){
			    System.out.println(line);
			}
			s.close();
		    } catch (IOException ex){
		    }
		}
	    }.start();
	
	} catch( IndexOutOfBoundsException ex) {
	} finally{

	}
    }

}

