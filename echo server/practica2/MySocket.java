package practica2;

import java.io.IOException;
import java.net.Socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.PrintWriter;
import java.io.OutputStreamWriter;

import java.lang.String;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MySocket {

    Socket s;
    BufferedReader br; 
    PrintWriter pw;

    public MySocket(int port){
        try{
            this.s = new Socket("127.0.0.1", port);
            br = new BufferedReader( new InputStreamReader( s.getInputStream() ) ); 
            pw = new PrintWriter( s.getOutputStream(),true );
        } catch( IOException ex){
	    ex.printStackTrace();
        }
    }
    
    public MySocket(String host, int port){
        try{
            this.s = new Socket(host, port);
            br = new BufferedReader( new InputStreamReader( s.getInputStream() ) ); 
            pw = new PrintWriter( s.getOutputStream(), true );
        } catch( IOException ex){
	    ex.printStackTrace();
        }
    }

    public MySocket(Socket s){
        try{
            this.s = s;
            br = new BufferedReader( new InputStreamReader( s.getInputStream() ) ); 
            pw = new PrintWriter( s.getOutputStream(), true );
        } catch( IOException ex) {
	    ex.printStackTrace();
        }
    }

    public Socket getSocket(){
	return s;
    }

    public  String readLine(){
	try{
	    return br.readLine();
	} catch ( IOException ex) {
	    ex.printStackTrace();
	    return "error";
	}
    }
    
    public void close() throws IOException{
        pw.close();
        br.close();
        s.close();
    }

    public void println(String line){
        pw.println(line);
    }
}
