package practica2;

import java.io.*;
import practica2.MyServerSocket;
import practica2.MySocket;
//import java.lang.Thread;

public class Server {

    public static void main(String[] args) {
        try{
            MyServerSocket ss = new MyServerSocket(Integer.parseInt(args[1]));
            //Keyboard thread//
            while (true) {
                MySocket s = ss.accept();
		System.out.println("ClientConnected!");
		String line = "";
		try{
		    while((line = s.readLine()) != null ){
			System.out.println(line);
			s.println(line);
		    }
		    s.close();
		} catch (IOException ex) {
		    ex.printStackTrace();
		    System.out.println("Error!");
		}
		
	    }
	} catch (IndexOutOfBoundsException ex) {
	    System.out.println("Usage: <nick> <port> ex: John 2222");
	} finally {
            
	}
    }
}
