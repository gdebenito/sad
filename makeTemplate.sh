#!/bin/bash

#File-Name:   makeTemplate.sh
#Date:        2017 Apr 24
#Author:      Gonzalo De Benito
#Purpose:     Make template file for java 
#Data Used:   Arguments (name, extension)
#Output File: name.extension
#Data Output: Programming basics style

if [ -n $1 ] #first argument isn't zero length
then
    FILENAME=$1 # hello.java
    arrIN=(${FILENAME//./ }) #parse strings by dot
    CLASS=${arrIN[0]} #CLASS NAME is next string element in array {hello , java}
    EXT_TYPE=${arrIN[1]} #EXTENSION TYPE

    if [ $EXT_TYPE = "java" ]
    then
	printf "/*\n"              > $FILENAME
	printf "  File-Name:\n"   >> $FILENAME
	printf "  Date:\n"        >> $FILENAME
	printf "  Author:\n"      >> $FILENAME
	printf "  Purpose:\n"     >> $FILENAME
	printf "  Data Used:\n"   >> $FILENAME
	printf "  Data Output:\n" >> $FILENAME
	printf "*/\n"             >> $FILENAME
	printf "package directory;\n//import java.util.*;\n//import java.io.*;\n//import java.lang.*;\n\n\n" >> $FILENAME
	printf "public class $CLASS{\n" >> $FILENAME
	printf "    public $CLASS(){\n\n" >> $FILENAME
	printf "    }\n\n" >> $FILENAME
	printf "    public static void main(String[] args){\n\n" >> $FILENAME
	printf "    }\n\n" >> $FILENAME
	printf "}" >> $FILENAME
	
    fi
fi


