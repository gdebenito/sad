import java.io.*;
import java.lang.*;
import java.util.*;

class HashMapDemo {


    public static void main( String[] args) {

	// create hash map
	HashMap<String,String> newmap = new HashMap<>();
      
	// populate hash map
	newmap.put("John", "socket1");
	newmap.put("Joshua", "socket2");
	newmap.put("Isaac", "socket3");
      
	// create set view for the map
	Set set=newmap.entrySet();
	Iterator i = set.iterator();

	System.out.println("Map Entry and Set");

        while( i.hasNext() ){
	    Map.Entry mapEntry = (Map.Entry)i.next();
	    System.out.println(mapEntry.getValue());
	}
      
	// check set values
	System.out.println("Set values: " + set);

	Iterator j = newmap.keySet().iterator();

	System.out.print("Key values: ");
	while(j.hasNext()){
	    String keys= (String)j.next();
	    System.out.print(keys + " " );
	}
	System.out.println();

	j = newmap.keySet().iterator();

	System.out.print("Key values but one like Joshua:");
	while(j.hasNext()){
	    String keys= (String)j.next();
	    if( keys != "Joshua")
		System.out.print(keys + " " );
	    else
		System.out.print("_" + keys + "_" );
	}
	System.out.println();

	
    }    
}
