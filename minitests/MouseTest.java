 package minitests;
 
 import java.io.InputStreamReader;
 //import java.io.BufferedReader;
 import java.io.IOException;
 import java.lang.Runtime;
 import java.lang.Process;
 import java.util.Scanner;
 import java.lang.String;
 //import java.io.*;
 
 
 public class MouseTest{
 
        static final int SET_X10_MOUSE = 9;
        static final int SET_VT200_MOUSE = 1000;
        static final int SET_VT200_HIGHLIGHT_MOUSE = 1001;
        static final int SET_BTN_EVENT_MOUSE = 1002;
        static final int SET_ANY_EVENT_MOUSE = 1003;

        static final int SET_FOCUS_EVENT_MOUSE = 1004;

        static final int SET_EXT_MODE_MOUSE = 1005;
        static final int SET_SGR_EXT_MODE_MOUSE = 1006;
        static final int SET_URXVT_EXT_MODE_MOUSE = 1015;

        static final int SET_ALTERNATE_SCROLL = 1007;
    
    public static void main( String[] args ){
    
        
    
        Scanner sc = new Scanner(new InputStreamReader(System.in));
        String a = "";
        
        
        
        //executeCommand("stty raw -echo </dev/tty");
        
        // DECSET CSI ? Pm h 
        System.out.print( "\011\033[?" + "9" + "h" + "\n");  //SET_X10_MOUSE
        
        sc.next();
        
        //DECRST CSI ? Pm l
        System.out.print( "\011\033[?" + "9" + "l" + "\n");  //UN-SET_X10_MOUSE
            
        sc.close();
        
        /*
        On button press, xterm sends CSI M CbCxCy (6 characters).
        o Cb is button-1.
        o Cx and Cy are the x and y coordinates of the mouse when the button was
        pressed.
        */
        /*
         xterm sends CSI M CbCxCy.
        o The low two bits of Cb encode button information: 0=MB1 pressed, 1=MB2
        pressed, 2=MB3 pressed, 3=release.
        o The next three bits encode the modifiers which were down when the but-
        ton was pressed and are added together:  4=Shift, 8=Meta, 16=Control.
        Note however that the shift and control bits are normally unavailable
        because xterm uses the control modifier with mouse for popup menus,
        and the shift modifier is used in the default translations for button
        events.  The Meta modifier recognized by xterm is the mod1 mask, and
        is not necessarily the "Meta" key (see xmodmap).
        */
        
    
    }
    
    
    public static void executeCommand(String cmd){
         try {
            Process process = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c" , cmd });
            process.waitFor();
        } catch (IOException ex1) {
            //Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex1);
        } catch (InterruptedException ex2) {
            //Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex2);
        }
    }
    
 
 
 
 }
