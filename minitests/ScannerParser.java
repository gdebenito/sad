
package minitests;

import java.io.*;
import java.util.Scanner;
import java.lang.String;

public class ScannerParser{

    public static void main(String[] args){
        
        System.out.print("\033[18t");
        Scanner sc = new Scanner(System.in);
        String a = sc.findWithinHorizon("[\\d]*;[\\d]*t",0); 
        
        //Result is CSI  8  ;  height ;  width t
        
        
        System.out.println("-------------");
        System.out.println("ROWS:" + a.split(";")[0]);
        System.out.println("COLS:" + a.split(";")[1].split("t")[0]);
    }
}

