package minitests;

import java.io.*;
import java.lang.String;
import java.util.regex.Pattern;

public class SplitProva {
    public static void main(String[] args){
        String input = "2;30R";
        
        String[] parts1 = input.split(";");
        
        //String[] parts2 = s.split("[\p{;}]"); //minitests/SplitProva.java:12: error: illegal escape character 
        
        Pattern p = Pattern.compile(";");
        String[] parts3 = p.split(input);
        
        System.out.println("String split:");
        for (String i : parts1){
            System.out.println(i);
        }
        
        System.out.println("Pattern Split:");
        for (String i : parts3){
            System.out.println(i);
        }
        
        /*
            Output:
            
            String split:
            2
            30R
            Pattern Split:
            2
            30R
            
        */
        
        
        
        /*
        for (String i : parts1){
            System.out.print(i);
        }*/
    }

}
