package mvc_practica1;

import java.io.*;
import mvc_practica1.*;
import java.util.*;

/**
 *
 * @author Gonzalo De Benito
 */
public class Console implements Observer{
    
    protected Line line;
    protected EditableBufferedReader ebr;
    protected int   term_cols,          term_rows,
                    cursor_cols,        cursor_rows,
                    init_cursor_cols,   init_cursor_rows,
                    end_cursor_cols,    end_cursor_rows;
    
    public Console(Line line, EditableBufferedReader ebr){
        this.line = line;
        this.ebr = ebr;
        this.reset();
        
        updateTerminalSize();
        setInitialCursorPosition();
    }
    
    @Override
    public void update( Observable o, Object arg){
        
        /*int i = (int)arg;
        System.out.print(String.valueOf(arg));*/
        
        int a = (int) arg;
        
        switch( a ) {
        
            case Datos.UP:
                 System.out.print(Datos.CSI +"1A");
            break;
        
            case Datos.DOWN:
                System.out.print(Datos.CSI + "1B");
            break;
            
            case Datos.LEFT:
                updateCursorPosition();
                if(isCursorFirstColumn()) cursorGoTo(cursor_rows - 1, term_cols); 
                else System.out.print(Datos.CSI +"D");
            break;
            
            case Datos.RIGHT:
                updateCursorPosition();
                updateEndCursorPosition();
                if ( isCursorLastColumn()) cursorGoTo(cursor_rows + 1, 0);
                else System.out.print(Datos.CSI +"C");
            break;
            
            case Datos.HOME:
                cursorGoTo(init_cursor_rows ,init_cursor_cols );
            break;
            
            case Datos.END:
                updateEndCursorPosition();
                cursorGoTo(end_cursor_rows, end_cursor_cols);
            break;
            
            case Datos.DELETE:
                System.out.print(Datos.CSI + "1P");
            break;
            
            case Datos.ERASE:
                System.out.print(Datos.CSI + "1X");
            break;
            
            case Datos.DEL_MULTI:
                System.out.print(Datos.CSI + "1P");
                updateCursorPosition();
                updateEndCursorPosition();
                if(line.getLineLength() >= term_cols && !isCursorInLastRow() ){ 
                    
                    saveCursorPosition();
                    
                    System.out.print(Datos.CSI + term_cols + "G");//end of line
                    int aux = (cursor_rows-init_cursor_rows+1)*term_cols;
                    System.out.print("" + line.getCharAt(aux-1));
                    
                    aux += term_cols;
                    while ( aux <= (end_cursor_rows - init_cursor_rows+1)*term_cols ){ 
                    
                        System.out.print(Datos.CSI + "0G" + Datos.CSI + "1B" + Datos.CSI + "1P");
                        
                        if( aux != (end_cursor_rows - init_cursor_rows+1)*term_cols )
                            System.out.print(Datos.CSI + term_cols + "G"  + line.getCharAt(aux-1) );
                            
                        aux += term_cols;
                    }
                    restoreCursorPosition();
                }
            break;
            
            case Datos.MOUSE:
                cursorGoTo( line.getMouseRows() + 1 , line.getMouseCols() +1 );
            break;
            
            default:
                
                if (line.insertState){
                    
                    updateCursorPosition();
                    if( isCursorLastColumn() ) cursorGoTo(cursor_rows+1,0);
                    
                    System.out.print(Datos.CSI + "1@" + (char) a );
                    
                    updateCursorPosition();
                    updateEndCursorPosition();
                        
                    if( line.getLineLength() > term_cols && !isCursorInLastRow() ){
                    
                        saveCursorPosition();
                        int aux = (cursor_rows-init_cursor_rows +1)*term_cols ;
                        
                        while ( aux < line.getLineLength()){ 
                            System.out.print(Datos.CSI + "0G" + Datos.CSI + "1B" );
                            
                            if ( aux != line.getLineLength()) System.out.print(Datos.CSI + "1@");
                            
                            System.out.print("" + line.getCharAt(aux));
                            aux += term_cols;
                        }
                        
                        restoreCursorPosition();
                    }
                    
                    /* Other implementation but don't work
                    updateCursorPosition();
                    if( line.isCursorLastColumn() ) cursorGoTo(cursor_rows+1,0);
                    System.out.print(Datos.CSI + "1@" + (char) a );
                    updateCursorPosition();
                    updateEndCursorPosition();
                    if(!line.isCursorInLastRow()){
                        saveCursorPosition();
                        int i=0;
                        for(i = (cursor_rows + 1); (i*term_cols+1)/term_cols <= line.end_cursor_rows ; i++){
                            cursorGoTo(i,0);
                            System.out.print(Datos.CSI + "1@" + line.getCharAt((i-init_cursor_rows)*term_cols) );
                        }
                        if ( i == line.end_cursor_rows) {
                            cursorGoTo(line.end_cursor_rows,0);
                            System.out.print(Datos.CSI + "1@" + line.getCharAt((i-1)*term_cols-1));
                        }
                        restoreCursorPosition();
                    }   */
                    
                    
                } else {
                    System.out.print(String.valueOf((char) a));  
                
                }
            break;
            
        
        
        }
        
        
    }
    
    public void updateCursorPosition(){
        ArrayList<Integer> arr = ebr.getCursor();
        cursor_rows = arr.get(0);
        cursor_cols = arr.get(1);
    }
    
    public void updateTerminalSize(){
        ArrayList<Integer> arr = ebr.getTerminalSize();
        term_rows = arr.get(0);
        term_cols = arr.get(1);
        line.term_cols = this.term_cols;
        line.term_rows = this.term_rows;
    }
    
    public void updateEndCursorPosition(){
        end_cursor_rows = init_cursor_rows + (line.getLineLength()/term_cols);
        end_cursor_cols = init_cursor_cols + (line.getLineLength()%term_cols);
    }
    
    public void reset(){
        
        eraseDisplay();
        cursorGoTo(0,0);
        setInitialCursorPosition();
    }
    
    public void setInitialCursorPosition(){
        ArrayList<Integer> arr = ebr.getCursor();
        init_cursor_rows = arr.get(0);
        init_cursor_cols = arr.get(1);
    }
    
    protected static void eraseDisplay(){
        System.out.print(Datos.CSI + "2J");
    }
    
    public static void cursorGoTo( int row, int col){
        System.out.print(Datos.CSI + row + ";" + col + "f");
    }
    
    public void saveCursorPosition(){
        System.out.print("\0337");
    }
    
    public void restoreCursorPosition(){
        System.out.print("\0338");
    }
    
    public boolean isCursorLastColumn(){
        return (cursor_cols == term_cols+1 );
    }
    
    public boolean isCursorFirstColumn(){
        return (cursor_cols == 1 );
    }
    
    public boolean isCursorInLastRow(){
        return (cursor_rows == end_cursor_rows);
    }
    
    //TESTING PURPOSES
    
    public String cursorPositionToString() {
        return "R:"+ cursor_rows +" C:" + cursor_cols ;
    }
    
    public String terminalSizeToString() {
        return "R:"+ term_rows +" C:" + term_cols ;
    }
    
}
