package mvc_practica1;

public class Datos { 

    public static final int BACKSPACE  = 127;
    public static final int UP         = 1000;
    public static final int DOWN       = 1001;
    public static final int LEFT       = 1002;
    public static final int RIGHT      = 1003;
    public static final int HOME       = 1004;
    public static final int END        = 1005;
    public static final int INSERT     = 1006;
    public static final int SUPR       = 1007;
    public static final int AVPAG      = 1008;
    public static final int REPAG      = 1009;
    
    public static final int DELETE     = 1010; //delete char and position
    public static final int ERASE      = 1011; //delete char but position
    public static final int DEL_MULTI  = 1012; //delete consequents chars from multiline
    
    public static final int MOUSE      = 1013;
    
    
    public static final String CSI     = "\033[";

}
