package mvc_practica1;

import java.io.*;
import java.lang.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.StringBuilder;
import java.util.Scanner;
import java.lang.String;
import java.util.ArrayList;

import mvc_practica1.Line;
import mvc_practica1.Console;


/**
 *
 * @author Gonzalo De Benito
 */
public class EditableBufferedReader {

    protected static Process process; // need to implement process. builder
    protected Line line;
    protected Scanner sc;

    public EditableBufferedReader(Reader arg0) {
        setRaw();
        sc = new Scanner(arg0);
        
        this.line = new Line();
        
        enableMouse();
        
        Console console = new Console(this.line, this);
        line.addObserver(console);
        
        
    }	
    
    public void enableMouse (){
        System.out.print( "\011\033[?" + "9" + "h" + "\n");
    }
    
    public void disableMouse (){
        System.out.print( "\011\033[?" + "9" + "l" + "\n");
    }
    
    public static void setRaw(){
        executeCommand("stty raw -echo </dev/tty");
    }

    public static void unsetRaw(){
        executeCommand("stty -raw echo </dev/tty");
    }
    
    public ArrayList<Integer> getTerminalSize(){
        
        System.out.print("\033[18t");
            
        String tsize = sc.findWithinHorizon("[\\d]*;[\\d]*t",0);
        ArrayList<Integer> arr = new ArrayList<Integer>();
        
        if (tsize != null){
            arr.add( Integer.valueOf(tsize.split(";")[0]) );
            arr.add( Integer.valueOf(tsize.split(";")[1].split("t")[0]) );
            
            //System.out.print("Row:" + arr.get(0) + " Col:" + arr.get(1));
        }
        
        return arr;
        //Result is CSI  9  ;  height ;  width t
        
        
    }
    
    public ArrayList<Integer> getCursor(){

        System.out.print("\033[6n");
        ArrayList<Integer> arr = new ArrayList<Integer>();
        
        
        String size = sc.findWithinHorizon("[\\d]*;[\\d]*R",0);
            
        if (size != null){
            arr.add( Integer.valueOf(size.split(";")[0]));
            arr.add( Integer.valueOf(size.split(";")[1].split("R")[0]));
            
            //System.out.print("Row:" + arr.get(0) + " Col:" + arr.get(1));
        }
            return arr;
        
        //CSI r ; c R     -->  r row c column

    }
    
    public static void executeCommand(String cmd){
         try {
            process = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c" , cmd });
            process.waitFor();
        } catch (IOException ex1) {
            Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex1);
        } catch (InterruptedException ex2) {
            Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex2);
        }
    }
    
    public int read() throws IOException{
        
        //  Scanner 
        int a = 0;
        char b;

        sc.useDelimiter("");
        
        //needs to be optimize or change methods. rudimentary, i don't like sc.next().charAt(0))
        
        switch( (int)(b = sc.next().charAt(0)) ){
            
            case 27: //ESC
            switch( (int) (b = sc.next().charAt(0)) ){ 

                case '[':
                switch ( (int) (b = sc.next().charAt(0)) ){

                    case 'A': a = Datos.UP;
                    break;

                    case 'B': a = Datos.DOWN;
                    break;

                    case 'C': a = Datos.RIGHT;
                    break;

                    case 'D': a = Datos.LEFT;
                    break;
                    
                    case 'M': 
                        switch( (int)(b = sc.next().charAt(0)) ){//on left click
                            case 32: //left button MB1
                                //System.out.print("HELLO");
                                line.setMouseCols( (int)sc.next().charAt(0) );
                                line.setMouseRows( (int)sc.next().charAt(0) );
                                a = Datos.MOUSE;
                                break;
                            case 33: //right button MB2
                            case 34: //middle button MB3
                            case 35: //release button
                            default:
                            break;
                        }
                            //a = Datos.MOUSE;
                    break;

                    case '2': if(sc.next().charAt(0)=='~') a =  Datos.INSERT;
                    break;

                    case '3': if(sc.next().charAt(0)=='~') a =  Datos.SUPR;
                    break;
                    
                    case '5': if(sc.next().charAt(0)=='~') a =  Datos.REPAG;
                    break;

                    case '6': if(sc.next().charAt(0)=='~') a =  Datos.AVPAG;
                    break;
                }
                break;

            
                case 'O':
                switch ((int)(b = sc.next().charAt(0)) ){

                    case 'H': a = Datos.HOME;
                    break;

                    case 'F': a = Datos.END;
                    break;
                }
                break;
            }
            break;
            
            default:
                a = (int)b;
            break;
        }
        return a;
       
    }

    public String readLine() throws IOException{
        int b = 0;
        
        
        while((b = this.read()) != 13){
        
            switch(b){
                
                case Datos.UP:        line.up();
                break;
                
                case Datos.DOWN:      line.down();
                break;
                
                case Datos.AVPAG:
                case Datos.REPAG:     //do nothing;
                break;
                
                case Datos.BACKSPACE: line.backspace();
                break;
                
                case Datos.SUPR:      line.supr();
                break;
                
                case Datos.HOME:      line.home();
                break;
                
                case Datos.END:       line.end();
                break;
                
                case Datos.RIGHT:     line.right();
                break;
                
                case Datos.LEFT:      line.left();
                break;
                
                case Datos.INSERT:    line.insert();
                break;
                
                case Datos.MOUSE:     line.mouse();
                break;
                
                /*
                case '*':       
                    line.updateCursorPosition();
                    System.out.print(line.getCursorPosition());
                break;
                
                case '/':       
                    line.updateTerminalSize();
                    System.out.print(line.getTerminalSize());
                break;
                */
                
                default:        line.addChar((char)b);
                break;
            }
           
        }
        
        line.end(); //the line wont overwrite the message
        
        disableMouse();
        
        return line.toString();
    }

    public static void main(String[] args){
        
        /* Get cols example
        try{
            int a = getCols();
            System.out.println("COLUMNS: " + a);
        } catch (IOException ex){
            System.out.println("ERROR");
        }
        */
        
    
    }
    
}
