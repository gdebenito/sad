package mvc_practica1;

import java.io.*;
import java.util.*;
import java.lang.StringBuilder;
import mvc_practica1.*;
import java.util.ArrayList;

/**
 *
 * @author Gonzalo De Benito
 */
public class Line extends Observable{  ///MODEL///

	protected int index,term_cols,term_rows;
	protected int mouse_cols,mouse_rows;
	protected boolean insertState;
    private StringBuilder sb;
	
	
	public Line(){
		index = 0;
		mouse_cols = 0;
		mouse_rows = 0;
		insertState = false;
		sb = new StringBuilder("");

	}
	
	public void addChar(char c){ //VIEW

		if( insertState ){
		
            sb.insert(index,c);
            index++;
            
            setChanged();
            notifyObservers( (int)c  );
            
        } else {

            if(index >= sb.length()) sb.append(c);
            else sb.setCharAt(index,c);
            index++;
            
            setChanged();
            notifyObservers( (int)c );
            
        }
        
        
	}

	public String toString(){ //VIEW NO NEED
		return this.sb.toString();
	}
	
	public char getCharAt(int a){  //method for view
        return sb.charAt(a);
	}
	
	public int getLineLength(){  //method for view
        return sb.length();
	}
	
	public void setMouseCols(int a){  
        mouse_cols = a - '!';
	}
	
	public void setMouseRows(int a){ 
        mouse_rows = a - '!';
	}
	
	public int getMouseCols(){  
        return mouse_cols;
	}
	
	public int getMouseRows(){ 
        return mouse_rows;
	}
	
    public void insert(){ //VIEW NO NEED
        insertState = !insertState;
    }
    
    public void up(){  //VIEW
        
        if((index - term_cols) >= 0 ) {
            index -= term_cols;
            setChanged();
            notifyObservers(Datos.UP);
        } else if (index < term_cols) {
            home(); 
        }
    }
    
    public void down(){  //VIEW
        
        if((index + term_cols) <= sb.length() ) {
            index += term_cols;
            setChanged();
            notifyObservers(Datos.DOWN);
        } else if (index < term_cols || index + term_cols > sb.length()) {
            end(); 
        } 
        
    }
    
    public void left(){  //VIEW
    
        if(index > 0 ){
        
            index--;
            
            setChanged();
            notifyObservers(Datos.LEFT);
        }
    }
    
    public void right(){  //VIEW
        
        if(index < sb.length()) {
        
            index++;
            
            setChanged();
            notifyObservers(Datos.RIGHT);
        }
    }
    
    public void backspace(){  //VIEW
        if(insertState){
            if (index > 0){
                
                this.left();
                sb.deleteCharAt(index);
                sb.trimToSize();
                
                setChanged();
                notifyObservers(Datos.DEL_MULTI);
            }
            
        } else {
            if(index < sb.length()) { 
                
                sb.setCharAt(index,' ');
                
                setChanged();
                notifyObservers(Datos.ERASE);
                
            }
        }
    }
    
    public void supr() {   //VIEW
        if(insertState){
            if(index < sb.length()) {
                
                sb.deleteCharAt(index);
                sb.trimToSize();
                
                setChanged();
                notifyObservers( Datos.DEL_MULTI );
            }
        } else {
            if(index < sb.length()) { 
                
                sb.setCharAt(index,' ');
                
                setChanged();
                notifyObservers( Datos.ERASE );
            }
        }
    }
    
    public void home(){  //VIEW
    
        index = 0;
        
        setChanged();
        notifyObservers(Datos.HOME);
    }
    
    public void end(){  //VIEW
        
        index = sb.length();
        
        setChanged();
        notifyObservers(Datos.END);
       
    }
    
    public void mouse(){  //VIEW
        
        int cond = mouse_rows*term_cols + mouse_cols;
        if( cond < sb.length()){
            index = cond;
            
            setChanged();
            notifyObservers(Datos.MOUSE);
        }
       
    }

}
