package mvc_practica1;

import java.io.*;
import java.util.*;
import java.lang.Integer;
import mvc_practica1.*;

class TestReadLine {
    public static void main(String[] args) {
        EditableBufferedReader in = new EditableBufferedReader(new InputStreamReader(System.in));
        
        String str = null;
        int a = 0;
        try {
            str = in.readLine();
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.print("\n\rline is: \n\r" + str);
        EditableBufferedReader.unsetRaw();
        System.out.print("\n\r");
    }
}
