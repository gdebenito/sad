package practica1;

import java.io.*;
import java.lang.Runtime;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.lang.StringBuilder;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.lang.String;
import java.util.ArrayList;

import practica1.Line;


/**
 *
 * @author lsadusr17
 */
public class EditableBufferedReader extends BufferedReader /*implements Observer*/{

    protected static Process process; // need to implement process. builder
    protected Line line;
    protected Scanner sc = new Scanner ( System.in );

    private static final int BACKSPACE  = 127;
    private static final int UP         = 1000;
    private static final int DOWN       = 1001;
    private static final int LEFT       = 1002;
    private static final int RIGHT      = 1003;
    private static final int HOME       = 1004;
    private static final int END        = 1005;
    private static final int INSERT     = 1006;
    private static final int SUPR       = 1007;
    private static final int AVPAG      = 1008;
    private static final int REPAG      = 1009;
    private static final String CSI     = "\033[";

    /** Create a EditableBufferedReader. Implements Line editing.
    * Set terminal in raw.
    * @param arg0 inputstream reader
    */
    public EditableBufferedReader(Reader arg0) {
        super(arg0);  //simpleparser
        //this.line = new Line();
        setRaw();
        //  sc =  ; //scannerparser
        this.line = new Line(this);
    }	

	/*
	
	public void update(Observable o, Object arg){
	
	}  
	
	
	*/
	

    /** Sets terminal in raw mode, without echo.
    */
    public static void setRaw(){
        executeCommand("stty raw -echo </dev/tty");
    }

    /** Sets terminal in cooked mode, with echo.
    */
    public static void unsetRaw(){
        executeCommand("stty -raw echo </dev/tty");
    }
    
    public ArrayList<Integer> getTerminalSize(){
        
        System.out.print("\033[18t");
            
        String tsize = sc.findWithinHorizon("[\\d]*;[\\d]*t",0);
        ArrayList<Integer> arr = new ArrayList<Integer>();
        
        if (tsize != null){
            arr.add( Integer.valueOf(tsize.split(";")[0]) );
            arr.add( Integer.valueOf(tsize.split(";")[1].split("t")[0]) );
            
            //System.out.print("Row:" + arr.get(0) + " Col:" + arr.get(1));
        }
        
        return arr;
        //Result is CSI  9  ;  height ;  width t
        
        
    }
    
    public ArrayList<Integer> getCursor(){

        System.out.print("\033[6n");
        ArrayList<Integer> arr = new ArrayList<Integer>();
        
        
        String size = sc.findWithinHorizon("[\\d]*;[\\d]*R",0);
            
        if (size != null){
            arr.add( Integer.valueOf(size.split(";")[0]));
            arr.add( Integer.valueOf(size.split(";")[1].split("R")[0]));
            
            //System.out.print("Row:" + arr.get(0) + " Col:" + arr.get(1));
        }
            return arr;
        
        //CSI r ; c R     -->  r row c column

    }
    
    
    public static void executeCommand(String cmd){
         try {
            process = Runtime.getRuntime().exec(new String[] {"/bin/sh", "-c" , cmd });
            process.waitFor();
        } catch (IOException ex1) {
            Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex1);
        } catch (InterruptedException ex2) {
            Logger.getLogger(EditableBufferedReader.class.getName()).log(Level.SEVERE, null, ex2);
        }
    }

    @Override
    public int read() throws IOException{ /* METODE 1 - Tecniques de Parsing*/

        int a = 0;
        
        switch( ( a = super.read()) ){    
            
            case 27: //ESC
            switch((a=super.read())){ 

                case '[':
                switch ((a=super.read())){

                    case 'A': a = UP;
                    break;

                    case 'B': a = DOWN;
                    break;

                    case 'C': a = RIGHT;
                    break;

                    case 'D': a = LEFT;
                    break;

                    case '2': if((super.read())=='~') a =  INSERT;
                    break;

                    case '3': if((super.read())=='~') a =  SUPR;
                    break;
                    
                    case '5': if((super.read())=='~') a =  REPAG;
                    break;

                    case '6': if((super.read())=='~') a =  AVPAG;
                    break;
                }
                break;

            
                case 'O':
                switch ((a=super.read())){

                    case 'H': a = HOME;
                    break;

                    case 'F': a = END;
                    break;
                }
                break;
            }
            break;
        }
        return a;
        
        /*  Scanner implementation
        String b = "";
        
        switch( (int)( b = sc.next()) ){    
            
            case 27: //ESC
            if(sc.findwhthinHorizon("\033") == null) break;
            switch((int)( b = sc.next() )){ 

                case '[':
                switch ((int)(b = sc.next())){

                    case 'A': a = UP;
                    break;

                    case 'B': a = DOWN;
                    break;

                    case 'C': a = RIGHT;
                    break;

                    case 'D': a = LEFT;
                    break;

                    case '2': if((super.read())=='~') a =  INSERT;
                    break;

                    case '3': if((super.read())=='~') a =  SUPR;
                    break;
                    
                    case '5': if((super.read())=='~') a =  REPAG;
                    break;

                    case '6': if((super.read())=='~') a =  AVPAG;
                    break;
                }
                break;

            
                case 'O':
                switch ((a=super.read())){

                    case 'H': a = HOME;
                    break;

                    case 'F': a = END;
                    break;
                }
                break;
            }
            break;
        }
        return a;
        
        */
    }

    @Override
    public String readLine() throws IOException{
        int b = 0;
        
        while((b = this.read()) != 13){
        
            switch(b){
                
                case UP:        line.up();
                break;
                
                case DOWN:      line.down();
                break;
                
                case AVPAG:
                case REPAG:     //do nothing;
                break;
                
                case BACKSPACE: line.backspace();
                break;
                
                case SUPR:      line.supr();
                break;
                
                case HOME:      line.home();
                break;
                
                case END:       line.end();
                break;
                
                case RIGHT:     line.right();
                break;
                
                case LEFT:      line.left();
                break;
                
                case INSERT:    line.insert();
                break;
                /*
                case '*':       
                    line.updateCursorPosition();
                    System.out.print(line.getCursorPosition());
                break;
                
                case '/':       
                    line.updateTerminalSize();
                    System.out.print(line.getTerminalSize());
                break;*/
                
                
                default:        line.addChar((char)b);
                break;
            }
        }
        return line.toString();
    }

    public static void main(String[] args){
        
        /* Get cols example
        try{
            int a = getCols();
            System.out.println("COLUMNS: " + a);
        } catch (IOException ex){
            System.out.println("ERROR");
        }
        */
        
    
    }
}
