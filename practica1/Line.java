package practica1;

import java.io.*;
import java.lang.StringBuilder;
import practica1.EditableBufferedReader;
import java.util.ArrayList;


public class Line /* extends Observable */{

	private int index, 
                cursor_cols,    cursor_rows,    //cursor coordinates
                term_cols,      term_rows;      //terminal size
	private boolean insertState;
	private StringBuilder sb;
	private EditableBufferedReader ebr;
	
	private static final String CSI = "\033[";
	
	public Line( EditableBufferedReader ebr){
		index = 0;
		insertState = false;
		sb = new StringBuilder("");
		this.ebr = ebr;
		
		updateTerminalSize();
		updateCursorPosition();

	}
	
	public void addChar(char c){

		if( insertState ){

            sb.insert(index,c);
            System.out.print(CSI + "1@");

        } else {

            if(index >= sb.length()) sb.append(c);
            else sb.setCharAt(index,c);
        }

        System.out.print(c);
        index++;
	}

	public String toString(){
		return this.sb.toString();
	}
    
    public void insert(){
        insertState = !insertState;
    }
    
    public void up(){
        
        if((index - term_cols) >= 0 ) {
            index -= term_cols;
            System.out.print(CSI +"1A");
        } else if (index < term_cols) {
            home(); 
        }
    }
    
    public void down(){
        
        if((index + term_cols) <= sb.length() ) {
            index += term_cols;
            System.out.print(CSI + "1B");
        } else if (index < term_cols || index + term_cols > sb.length()) {
            end(); 
        } 
        
    }
    
    public void left(){
    //need multiline support
    
        if(index > 0 /*&& index < term_cols*/){ 
            index--;
            System.out.print(CSI +"D");
        }
    }
    
    public void right(){
    //need multiline support
    
        if(index < sb.length()) {
            index++;
            System.out.print(CSI +"C");
        }
    }
        
    
    public void backspace(){
    //need multiline support
        if(insertState){
            if(index < sb.length() && index > 0) {
                System.out.print(CSI + "D"); //move left
                System.out.print(CSI + "1P");//delete char
                index--;
                sb.deleteCharAt(index);
                sb.trimToSize();
            }
        } else {
            if(index < sb.length()) { 
                System.out.print(CSI + "1X");
                sb.setCharAt(index,' ');
            }
        }
    }
    
    public void supr() { 
    //need multiline support
        if(insertState){
            if(index < sb.length()) {
                System.out.print(CSI + "1P");
                sb.deleteCharAt(index);
                sb.trimToSize();
            }
        } else {
            if(index < sb.length()) { 
                System.out.print(CSI + "1X");
                sb.setCharAt(index,' ');
            }
        }
    }
    
    
    public void home(){
    //need multiline support
    
        if( index < term_cols){
            System.out.print(CSI + "0G");
            index = 0;
        } else if ( index >= term_cols){
            System.out.print(CSI + cursor_rows + ";" + cursor_cols + "f");
            index = 0;
        }

//        CSI Ps ; Ps f  //row//column
    }
    
    public void end(){
    //need multiline support
        if( sb.length() < term_cols ){
            System.out.print(CSI + (sb.length() + 1 ) + "G");
            index = sb.length();
        } else {
            int end_rows = cursor_rows + (sb.length()/term_cols);
            int end_cols = cursor_cols + (sb.length()%term_cols);
            System.out.print(CSI + end_rows + ";" + end_cols + "f");
            //System.out.println(cursor_rows + ";" + cursor_cols);
            //System.out.println( end_rows + ";" + end_cols);
            index = sb.length();
        }
    }
    
    ///////TERMINAL///////
    
    public String getCursorPosition() {
        return "R:"+ cursor_rows +" C:" + cursor_cols ;
    }
    
    public String getTerminalSize() {
        return "R:"+ term_rows +" C:" + term_cols ;
    }
    
    public void updateCursorPosition(){
        ArrayList<Integer> arr = ebr.getCursor();
        cursor_rows = arr.get(0);
        cursor_cols = arr.get(1);
    }
    
    public void updateTerminalSize(){
        ArrayList<Integer> arr = ebr.getTerminalSize();
        term_rows = arr.get(0);
        term_cols = arr.get(1);
    }
    
    
    /*
	public static void main(String[] args){
		//System.out.print("Hello Line");
		//Line line = new Line();
		line.addChar('H');
		line.addChar('I');
		System.out.println(line.toString());
	}
	*/


} 
