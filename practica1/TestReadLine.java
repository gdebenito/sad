package practica1;

import java.io.*;
import java.lang.Integer;
import practica1.EditableBufferedReader;

class TestReadLine {
    public static void main(String[] args) {
        BufferedReader in = new EditableBufferedReader(new InputStreamReader(System.in));
        String str = null;
        int a = 0;
        try {
            str = in.readLine();
            //a = in.read();
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        //System.out.print("\nline is: " + a);
        System.out.print("\n\n\n\n\rline is: \n\r" + str);
        
        // para facilitarnos el trabajo
        EditableBufferedReader.unsetRaw();
        System.out.print("\n\r");
    }
}
