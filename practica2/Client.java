/* File-Name:   Client.java
   Date:        2017 Apr 18
   Author:      Gonzalo De Benito
   Purpose:     Client of a chat. Has two threads, 
   Keyboard: read system.in and send to server
   Console: read from socket and print into screen.
   Data Used:   None
   Output File: Client.class
   Data Output: Console output
*/

package practica2;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import practica2.MyServerSocket;
import practica2.MySocket;



public class Client
{

    public static void main(String[] args)
    {
	
	try{
	    final MySocket s = new MySocket(args[0], Integer.parseInt(args[1]));
	    final String nick = args[2]; //nick
	    
	    //keyboard thread
	    new Thread(){
		public void run(){
		    String line;
		    try{
			BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
			while((line = bf.readLine() ) != null){ //EOF
			    s.println(line);
			}
			System.out.println("Disconnecting...");
			s.println(Comm.DISCONNECT);
		    } catch (IOException ex) {
			System.out.print("IOException NOT IMPLEMENTED");
		    }
		}
	    }.start();

	    //console thread
	    new Thread(){
		public void run(){
			
		    String line;
			
		    try {
			while ( (line = s.readLine()) != null) {
			    if (line.matches(Comm.NICK)) {
				s.println(nick);
			    } else if (line.matches(Comm.DISCONNECT)) {
				break; //disconnect client
			    } else {
				System.out.println(line);
			    }
			}
			s.close();
		    } catch (IOException ex) {
			ex.printStackTrace();
		    }
		}
	    }.start();
	} catch( IndexOutOfBoundsException ex) {
	    System.out.print( "Usage: <host> <port> <nick>; ex: localhost 2222 John");
	} finally {
	    // nothing to do
	}
    }

}

