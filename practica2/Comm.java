/*
  File-Name: Comm.java
  Date: 2017 Apr 25
  Author: Gonzalo De Benito
  Purpose: Make default communications
  Data Used: none
  Data Output: none
*/
package practica2;
//import java.util.*;
//import java.io.*;
import java.lang.String;


public class Comm
{

    public static final String NICK       = "NICK";
    public static final String ACK        = "ACK";
    public static final String DISCONNECT = "DISC";

    
    public Comm()
    {

    }
}
