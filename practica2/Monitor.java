package practica2;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.lang.String;

class Monitor {
    

    HashMap<String,MySocket> map;
    ReentrantLock lk;

    public Monitor() {
	 map = new HashMap<>();
	 lk = new ReentrantLock();
    }

    public void add(String nick, MySocket s) {
	lk.lock();
	map.put(nick,s);
	lk.unlock();
	sendAll(nick + " has connected to the chat");
	/*optional*/ //welcome(nick,s);
    }

    public void remove(String nick) { 
	lk.lock();
	map.remove(nick);
	lk.unlock();
    }

    public MySocket getSocketFrom(String nick) {
	lk.lock();
	try {
	    return map.get(nick);
	} finally {
	    lk.unlock();
	}
    }

    public void send(String nick, String line) {
	send(getSocketFrom(nick), line);
    }

    public void send(MySocket s, String line) {
	s.println(line);
    }

    public String receive(String nick) {
	return receive(getSocketFrom(nick));
    }
    
    public String receive(MySocket s)  {
	return s.readLine();
    }

    /** Send line to all users. */
    public void sendAll(String line) {
	lk.lock();
	Iterator i = map.keySet().iterator();
	lk.unlock();
	String user;
	System.out.println("All:" + line);    /*server view*/
	while(i.hasNext()){
	    send((String)i.next(), line);
	}
	    
    }

    /** Send line to all users but himself. */
    public void sendAllBut(String nick, String line) {
	lk.lock();
	Iterator i = map.keySet().iterator();
	lk.unlock();
	System.out.println(line);
	String user;
	while (i.hasNext()) {
	    if ((user = (String) i.next()) != nick) {
		send(user, line);
	    }
	}
    }

    public void sendMssg(String nick , String line) {
	sendAllBut(nick, nick + ": " + line);   /*John: Hello world!*/
    }

    /** Get nick from Client*/
    public String getNick(MySocket s) {
	send(s,Comm.NICK);
	return receive(s);
    }

    public void disconnect(String nick) {
	send(getSocketFrom(nick), Comm.DISCONNECT);
	sendAll(nick + " has disconnected.");
	remove(nick);
    }

    
    /** Optional function */
    public void welcome(String nick, MySocket s) {
	s.println("Welcome to the chat!");
	s.println("Users connected:");
	lk.lock();
	Iterator i = map.keySet().iterator();
	lk.unlock();
	String user;
	while (i.hasNext())
	    s.println((String) i.next());
	s.println("There are " + map.size() + " members right now.");
    }
}
