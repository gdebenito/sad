/* File-Name:   Process.java
   Date:        2017 Apr 18
   Author:      Gonzalo De Benito
   Purpose:     Send and recieves information for each Client.
   Data Used:   None
   Output File: Process.class
   Data Output: Console output
*/

package practica2;

import java.io.*;
import java.lang.*;
import java.util.*;

class Process extends Thread {

    Monitor m;
    MySocket s;
    String nick;
    
    public Process(Monitor mon, MySocket ms){
	m = mon;
	s = ms;
    }

    @Override
    public void run() {
	//retrieve nick
	nick = m.getNick(s);
	//add to hashmap
	m.add(nick, s);
	String line;
	//connected mode, disconnect if 
	while((line = m.receive(s)) != null){
	    if (line.matches(Comm.DISCONNECT)){
		m.disconnect(nick);
		try {
		    s.close();
		} catch ( IOException ex ){
		    ex.printStackTrace();
		}
		break;
	    } else {
		m.sendMssg(nick,line);
	    }
	}
    }
}
